package org.informatika.attendance.activity.ui.home

import android.provider.ContactsContract
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.informatika.attendance.model.Presence
import org.informatika.attendance.model.Staff
import org.informatika.attendance.model.UnitKerja
import org.informatika.attendance.util.FirebaseService

class HomeViewModel : ViewModel() {

    private val _staff = MutableLiveData<Staff?>(null)
    val staff: LiveData<Staff?> = _staff

    private val _unitKerja = MutableLiveData<UnitKerja>(null)
    val unitkerja: LiveData<UnitKerja?> = _unitKerja

    private val _presensi = MutableLiveData<Presence?>(null)
    val presensi: LiveData<Presence?> = _presensi

    init {
        viewModelScope.launch {
            FirebaseService.getUser().collect{ staff ->
                _staff.value = staff
                _unitKerja.value = FirebaseService.getUserUnitKerja(staff!!.unitKerja!!)
            }
        }

        fetchPresence()
    }

    fun fetchPresence(){
        viewModelScope.launch {
            _presensi.value = FirebaseService.getPresence()
        }
    }
}