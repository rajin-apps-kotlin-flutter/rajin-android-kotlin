package org.informatika.attendance.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import org.informatika.attendance.R
import org.informatika.attendance.databinding.ActivityMenuBinding

class MenuActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMenuBinding
    private var isAllFabsVisible: Boolean? = null
    private var alreadyCheckout: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_menu)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(setOf(
            R.id.navigation_home, R.id.navigation_presence, R.id.navigation_request))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        isAllFabsVisible = false

        // Now set all the FABs and all the action name texts as GONE
        binding.fabAddRequest.visibility = View.GONE
        binding.fabTakeAttendance.visibility = View.GONE

        // Set the Extended floating action button to shrinked state initially
        binding.fabClose.shrink()
        binding.fabClose.setOnClickListener {
            updateAllFabVisibility()
        }

        binding.fabTakeAttendance.setOnClickListener {
            updateAllFabVisibility()
            moveToTakeAttendanceActivity()
        }

        binding.fabAddRequest.setOnClickListener {
            updateAllFabVisibility()
            moveToAddRequestActivity()
        }
    }

    private fun updateAllFabVisibility() {
        isAllFabsVisible = if (!isAllFabsVisible!!) {
            binding.fabAddRequest.show()
            binding.fabClose.extend()
            if (!alreadyCheckout) {
                binding.fabTakeAttendance.show()
            }
            true
        } else {
            binding.fabTakeAttendance.hide()
            binding.fabAddRequest.hide()
            binding.fabClose.shrink()
            false
        }
    }

    private fun moveToAddRequestActivity() {
        val intent = Intent(this, RequestActivity::class.java)
        startActivity(intent)
    }

    private fun moveToTakeAttendanceActivity() {
        val intent = Intent(this, TakeAttendanceActivity::class.java)
        startActivity(intent)
    }
}