package org.informatika.attendance.model

data class User (
    var username: String,
    var password: String,
    var passwordConfirm: String
)